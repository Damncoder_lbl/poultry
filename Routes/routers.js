

const express = require('express')
const router = express.Router();




router.get('/',function(req,res){

    res.render('home')

})
router.get('/home',function(req,res){

  res.render('home')
})
router.get('/login',function(req,res){

res.render('login')

})
router.get('/signup',function(req,res){

  res.render('register')
  
  })
  
router.get('/forget',function(req,res){

    res.render('forget')
      
})

router.get('/dashboard',function(req,res){

       res.render('dashboard')
})
router.get('/data',function(req,res){

    res.render('data')
})

router.get('/records',function(req,res){

        res.render('records')

})
router.get('/approval',function(req,res){

        res.render('approval')
})
router.get('/LOD',function(req,res){

    res.render('LOD')
})

router.get('/gs',function(req,res){

    res.render('gs')
})

router.get('/cs',function(req,res){

    res.render('cs')    
      
})
router.get('/wset',function(req,res){

    res.render('wset')
      
})

router.get('/devset',function(req,res){

    res.render('devset')
})    


router.get('/uar',function(req,res){
    
    res.render('uar')
})

router.get('/profile',function(req,res){

    res.render('profile')
})
router.get('/Notifications',function(req,res){

    res.render('Notifications')
})

module.exports = router