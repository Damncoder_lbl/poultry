const Express = require('express')
const app = Express()
const routes = require('./Routes/routers')
const PORT= 4000

app.use(Express.static('public'))
app.set('view engine','ejs')
app.use(routes)

app.listen(PORT,function(err){
     if(err){
         console.log(err)
     }
    console.log("serve @ ",PORT)
})

